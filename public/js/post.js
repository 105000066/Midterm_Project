function init () {

    var browse = document.getElementById("browse");
    var post = document.getElementById("post");
    var profile = document.getElementById("profile");

    browse.addEventListener('click',function(){
        window.location = 'index.html';
    });

    post.addEventListener('click',function(){
        window.location = 'post.html';
    });

    profile.addEventListener('click',function(){
        var user = firebase.auth().currentUser;
        localStorage.setItem("useremail",user.email);
        localStorage.setItem('uid', user.uid);
        window.location = 'profile.html';
    });

    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            user_email = user.email;
            var logout_button = document.getElementById('logout-btn');
            logout_button.addEventListener('click', function () {
                firebase.auth().signOut()
                    .then(function () {
                        alert('Sign Out!')
                    })
                    .catch(function (error) {
                        alert('Sign Out Error!')
                    });
            });
        } else {
            window.location = "signin.html";
        }
    });

    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('content');
    post_title=document.getElementById('title');

    post_btn.addEventListener('click', function () {
        if (post_txt.value != "" && post_title.value !="") {
            var newpostref = firebase.database().ref('com_list').push();
            var userurl = firebase.auth().currentUser.photoURL;  
            console.log(userurl);
            newpostref.set({
                email: user_email,
                title: post_title.value,
                data: post_txt.value,
                picurl: userurl
            });
            post_txt.value = "";
            post_title.value ="";
            alert('done');
        }
    });
}

window.onload = function () {
    init();
}