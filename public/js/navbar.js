function init() {
    var browse = document.getElementById("browse");
    var post = document.getElementById("post");
    var profile = document.getElementById("profile");

    browse.addEventListener('click',function(){
        window.location = 'index.html';
    });

    post.addEventListener('click',function(){
        window.location = 'post.html';
    });

    profile.addEventListener('click',function(){
        window.location = 'profile.html';
    });

    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            user_email = user.email;
            var logout_button = document.getElementById('logout-btn');
            logout_button.addEventListener('click', function () {
                firebase.auth().signOut()
                    .then(function () {
                        alert('Sign Out!')
                    })
                    .catch(function (error) {
                        alert('Sign Out Error!')
                    });
            });
        } else {
            window.location = "signin.html";
        }
    });


}



window.onload = function () {
    init();
}

