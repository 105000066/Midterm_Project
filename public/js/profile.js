function init() {
    
    var browse = document.getElementById("browse");
    var post = document.getElementById("post");
    var profile = document.getElementById("profile");

    browse.addEventListener('click',function(){
        window.location = 'index.html';
    });

    post.addEventListener('click',function(){
        window.location = 'post.html';
    });

    profile.addEventListener('click',function(){
        var user = firebase.auth().currentUser;
        localStorage.setItem("useremail",user.email);
        localStorage.setItem('uid', user.uid);
        window.location = 'profile.html';
    });

    document.getElementById('useremail').innerText = localStorage.getItem("useremail");
    var newref = firebase.database().ref('user_data');
    newref.orderByChild("email").equalTo(localStorage.getItem("useremail")).on('value', function (snapshot0) {
        snapshot0.forEach(function(childSnapshot0) {
        var childData = childSnapshot0.val();
        document.getElementById('about').innerText = childData.about;
          });
        });

        var storageRef = firebase.storage().ref();
        var mountainsRef = storageRef.child(localStorage.getItem("useremail")+'.png');
        mountainsRef.getDownloadURL().then(function(url) {
            // Insert url into an <img> tag to "download"
            document.getElementById('userpic').src = url;
            console.log(url);
          })
    
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            user_email = user.email;
            if(user.email === document.getElementById('useremail').innerText)
            {
                showHideDiv('edit_about');
                showHideDiv('fileButton');
            }
            var logout_button = document.getElementById('logout-btn');
            logout_button.addEventListener('click', function () {
                firebase.auth().signOut()
                    .then(function () {
                        alert('Sign Out!')
                    })
                    .catch(function (error) {
                        alert('Sign Out Error!')
                    });
            });
        } else {
            window.location = "signin.html";
        }
    });
    var edit_about = document.getElementById('edit_about');
    edit_about.addEventListener('click',function(){
        var about = prompt('Please describe about yourself','');
        if(about != null)
        {
            document.getElementById('about').innerText = about;
        }
        else
        {
            return;
        }
        var user = firebase.auth().currentUser;
        var usersRef = firebase.database().ref('user_data').child(user.uid);
        usersRef.set({
            email:user.email,
            about:about
        });
    });

    var str_before_title = "<div class='my-3 p-3 bg-white rounded box-shadow post_box'><h4 class='border-bottom border-gray pb-2 mb-0'>";
    var str_after_title = "</h4></div>";
    var postsRef = firebase.database().ref('com_list');
    var total_post = [];
    var first_count = 0;
    var second_count = 0;
    postsRef.orderByChild("email").equalTo(user_email).once('value')
        .then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                var childData = childSnapshot.val();
                total_post[total_post.length] = str_before_title + childData.title + str_after_title ;
                first_count += 1;
            });
            document.getElementById('post_list').innerHTML = total_post.join('');

            //add listener
            postsRef.orderByChild("email").equalTo(user_email).on('child_added', function (data) {
                second_count += 1;
                if (second_count > first_count) {
                    var childData = data.val();
                    total_post[total_post.length] = str_before_title + childData.title + str_after_title;
                    document.getElementById('post_list').innerHTML = total_post.join('');
                }
            });
        })
        document.querySelector('body').addEventListener('click', function(event) {
            if (event.target.tagName.toLowerCase() === 'h4') {
                localStorage.setItem("posttitle",event.target.innerHTML);
                window.location = 'postpage.html';
            }
        });

        var fileButton = document.getElementById('fileButton');
        
        
        //Listen for file 
        fileButton.addEventListener('change', function(e){
  
           //Get File
           var file = e.target.files[0];
           var user = firebase.auth().currentUser;  
           //Create a Storage Ref
           var storageRef = firebase.storage().ref();

            // Create a reference to 'mountains.jpg'
            var mountainsRef = storageRef.child(user.email+'.png');
  
           //Upload file
           var uploadTask = mountainsRef.put(file);

// Listen for state changes, errors, and completion of the upload.
uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED, // or 'state_changed'
  function(snapshot) {
    // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
    var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
    console.log('Upload is ' + progress + '% done');
    switch (snapshot.state) {
      case firebase.storage.TaskState.PAUSED: // or 'paused'
        console.log('Upload is paused');
        break;
      case firebase.storage.TaskState.RUNNING: // or 'running'
        console.log('Upload is running');
        break;
    }
  }, function(error) {

  // A full list of error codes is available at
  // https://firebase.google.com/docs/storage/web/handle-errors
  switch (error.code) {
    
  }
}, function() {
  // Upload completed successfully, now we can get the download URL
  var downloadURL = uploadTask.snapshot.downloadURL;
  document.getElementById('userpic').src = downloadURL;
    var user = firebase.auth().currentUser;
    user.updateProfile({
    photoURL: downloadURL
    }).then(function() {
        alert('update success');
    }).catch(function(error) {
        alert('update fail');
    });
});
      });

}

window.onload = function () {
    init();
}

function showHideDiv(id) {
    var e = document.getElementById(id);
    if(e.style.display == null || e.style.display == "none") {
        e.style.display = "block";
    } else {
        e.style.display = "none";
    }
}