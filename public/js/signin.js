function initApp() {
    // Login with Email/Password
    var txtEmail = document.getElementById('email');
    var txtPassword = document.getElementById('pwd');
    var btnLogin = document.getElementById('btnLogin');
    var btnGoogle = document.getElementById('btngoogle');
    var btnfb = document.getElementById('btnfacebook');
    var btnSignUp = document.getElementById('btnSignUp');
    var emailup = document.getElementById('emailup');
    var pwdup = document.getElementById('pwdup');
    var username = document.getElementById('text');

    btnLogin.addEventListener('click', function () {
        var email = txtEmail.value;
        var password = txtPassword.value;
        firebase.auth().signInWithEmailAndPassword(email, password)
            .then(function () {
                window.location = "index.html";
            })
            .catch(function (error) {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                alert(errorMessage);
                txtEmail.value = "";
                txtPassword.value = "";
            });
    });

    btnGoogle.addEventListener('click', function () {
        var provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithPopup(provider)
            .then(function () {
                window.location = "index.html";
            })
            .catch(function (error) {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                alert( errorMessage);
                txtEmail.value = "";
                txtPassword.value = "";
            });
    });

    btnSignUp.addEventListener('click', function () {
        var email = emailup.value;
        var password = pwdup.value;
        firebase.auth().createUserWithEmailAndPassword(email, password)
            .then(function () {
                alert("Success! You could sign in  right now!");
                emailup.value = "";
                pwdup.value = "";
                username.value = "";
            })
            .catch(function (error) {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                emailup.value = "";
                pwdup.value = "";
                username.value = "";
            });
    });

}

window.onload = function () {
    initApp();
}