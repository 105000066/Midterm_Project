var user_email = '';
var temp;
var MyApp = {};

function init() {
    var postkey;
    var c_star = 1;
    var browse = document.getElementById("browse");
    var post = document.getElementById("post");
    var profile = document.getElementById("profile");

    browse.addEventListener('click',function(){
        window.location = 'index.html';
    });

    post.addEventListener('click',function(){
        window.location = 'post.html';
    });

    profile.addEventListener('click',function(){
        var user = firebase.auth().currentUser;
        localStorage.setItem("useremail",user.email);
        localStorage.setItem('uid', user.uid);
        window.location = 'profile.html';
    });

    
    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            user_email = user.email;
            var logout_button = document.getElementById('logout-btn');
            logout_button.addEventListener('click', function () {
                firebase.auth().signOut()
                    .then(function () {
                        alert('Sign Out!')
                    })
                    .catch(function (error) {
                        alert('Sign Out Error!')
                    });
            });
        } else {
            window.location = "signin.html";
        }
    });
    
    
    var posttitle = localStorage.getItem("posttitle");
    var postsRef = firebase.database().ref('com_list');
    document.getElementById('posttitle').innerHTML = posttitle;
    
    postsRef.orderByChild("title").equalTo(posttitle).on('value', function (snapshot0) {
        //postkey = snapshot.val();
        //go through each item found and print out the emails
        snapshot0.forEach(function(childSnapshot0) {
            postkey = childSnapshot0.key;
            console.log('postkey'+postkey);
            localStorage.setItem("textkey",postkey);
            console.log('textkey'+localStorage.getItem("textkey"));
            var childData = childSnapshot0.val();
            if(childData.picurl) document.getElementById('userurl').src = childData.picurl;
            else {
                document.getElementById('userurl').src = '/download.png';
                childData.picurl = 'download.png';
            }
            //this will be the actual email value found
            document.getElementById('postauthor').innerHTML = "<h7 style='font-weight:bold'>"+ childData.email+"</h7>";
            document.getElementById('postcontent').innerHTML = childData.data;
        });
        var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><div class='media text-muted pt-3'><img src=";
        var str_befor_img = " alt='' class='mr-2 rounded' style='height:40px; width:40px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><h7 class='d-block text-gray-dark' style='font-weight:bold'>";
        var str_after_content = "</p></div></div>\n";
        console.log(localStorage.getItem("textkey"));
        var compostsRef = firebase.database().ref('com_list/'+localStorage.getItem("textkey")+'/comments');
        temp = compostsRef;
        var first_count = 0;
        var second_count = 0;
        var total_post = [];
        compostsRef.once('value')
            .then(function (snapshot) {
                snapshot.forEach(function (childSnapshot) {
                    var childData = childSnapshot.val();
                    if(!childData.picurl) childData.picurl = '/download.png';
                    total_post[total_post.length] = str_before_username + childData.picurl+str_befor_img+childData.email + "</h7>" + childData.data + str_after_content;
                    first_count += 1;
                });
                document.getElementById('commentlist').innerHTML = total_post.join('');

                //add listener
                compostsRef.on('child_added', function (data) {
                    second_count += 1;
                    if (second_count > first_count) {
                        var childData = data.val();
                        if(!childData.picurl) childData.picurl = '/download.png';
                        total_post[total_post.length] = str_before_username +childData.picurl+str_befor_img+  childData.email + "</h7>" + childData.data + str_after_content;
                        document.getElementById('commentlist').innerHTML = total_post.join('');
                    }
                });
            })
            .catch(e => console.log(e.message));
    });
    
    var star = document.getElementById('star');
    var comment = document.getElementById('comment');
    star.addEventListener('click',function(){
        if(c_star){
            star.src = '/fillstar.png';
            c_star = 0;
        }
        else{
            star.src = '/emptystar.png';
            c_star = 1;
        }
    });

    comment.addEventListener('click',function(){
        showHideDiv('newcomment');
    })
    
    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment_text');
    post_btn.addEventListener('click', function () {
        if (post_txt.value != "") {
            //console.log(postkey);
            var userurl = firebase.auth().currentUser.photoURL;  
            var newpostref = firebase.database().ref('com_list/' + postkey+'/comments').push();
            newpostref.set({
                title: posttitle,
                email: user_email,
                data: post_txt.value,
                picurl: userurl
            });
            post_txt.value = "";
            alert('Submit Success!');
            showHideDiv('newcomment');
        }
    });

        
    

     document.querySelector('body').addEventListener('click', function(event) {
        if (event.target.tagName.toLowerCase() === 'h7') {
            localStorage.setItem("useremail",event.target.innerHTML);
            window.location = 'profile.html';
        }
    });
}

window.onload = function () {
    init();
}

function showHideDiv(id) {
    var e = document.getElementById(id);
    if(e.style.display == null || e.style.display == "none") {
        e.style.display = "block";
    } else {
        e.style.display = "none";
    }
}