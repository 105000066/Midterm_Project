function init() {
    var browse = document.getElementById("browse");
    var post = document.getElementById("post");
    var profile = document.getElementById("profile");

    browse.addEventListener('click',function(){
        window.location = 'index.html';
    });

    post.addEventListener('click',function(){
        window.location = 'post.html';
    });

    profile.addEventListener('click',function(){
        var user = firebase.auth().currentUser;
        localStorage.setItem("useremail",user.email);
        localStorage.setItem('uid', user.uid);
        window.location = 'profile.html';
    });

    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            user_email = user.email;

            var logout_button = document.getElementById('logout-btn');
            logout_button.addEventListener('click', function () {
                firebase.auth().signOut()
                    .then(function () {
                        alert('Sign Out!')
                    })
                    .catch(function (error) {
                        alert('Sign Out Error!')
                    });
            });
        } else {
            window.location = "signin.html";
        }
    });

    var str_before_title = "<div class='my-3 p-3 bg-white rounded box-shadow post_box'><h4 class='border-bottom border-gray pb-2 mb-0'>";
    var str_after_title = "</h4><div class='media text-muted pt-3'><img src=";
    var str_after_img =  " alt='' class='mr-2 rounded' style='height:50px; width:50px;'><div class='pb-3 mb-0 small lh-125'>";
    var str_after_username = "</div></div></div>\n"; 
/*
    var userref = firebase.database().ref('user_list').push();
    userref.set({
        email: user_email,
        star: 0
    });
*/
    var database = firebase.database();
    
    function writeUserData(email) {
    firebase.database().ref('users/' + userId).set({
        email: email
        //some more user data
        });
    }

    var postsRef = firebase.database().ref('com_list');
    var total_post = [];
    var first_count = 0;
    var second_count = 0;
    postsRef.once('value')
        .then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                
                var childData = childSnapshot.val();
                console.log(childData.picurl);
                if(!childData.picurl) childData.picurl = '/download.png';
                total_post[total_post.length] = str_before_title + childData.title + str_after_title +childData.picurl +str_after_img+ '<span class="useremail">'+ "<h7 style='font-weight:bold'>" +childData.email+"</h7>" +'</span>'+str_after_username;
                first_count += 1;
            });
            document.getElementById('post_list').innerHTML = total_post.join('');

            //add listener
            postsRef.on('child_added', function (data) {
                second_count += 1;
                if (second_count > first_count) {
                    var childData = data.val();
                    if(!childData.picurl) childData.picurl = '/download.png';
                    console.log(childData.picurl);
                    total_post[total_post.length] = str_before_title + childData.title + str_after_title +childData.picurl +str_after_img+ "<h7 style='font-weight:bold'>" +childData.email+"</h7>" +str_after_username;
                    document.getElementById('post_list').innerHTML = total_post.join('');
                }
            });
        })
        .catch(e => console.log(e.message));
        document.querySelector('body').addEventListener('click', function(event) {
            if (event.target.tagName.toLowerCase() === 'h4') {
                localStorage.setItem("posttitle",event.target.innerHTML);
                window.location = 'postpage.html';
            }
            if (event.target.tagName.toLowerCase() === 'h7') {
                localStorage.setItem("useremail",event.target.innerHTML);
                window.location = 'profile.html';
            }
        });
}

window.onload = function () {
    init();
}
