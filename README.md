# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* [Forum]
* Key functions (add/delete)
    1. user page
    2. post page
    3. post list page
    4. leave comment under any post
* Other functions (add/delete)
    1. google sign in
    2. user page show recent posts by user.
    3. added css animation(using WoW API)(http://mynameismatthieu.com/WOW/index.html)
    4. user profile picture support(change picture, uploadpicture)


## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|N|
|GitLab Page|5%|N|
|Database|15%|N|
|RWD|15%|N|
|Topic Key Function|15%|N|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|N|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|N|
|Security Report|5%|N|
|Other functions|1~10%|N|

## Website Detail Description
* firebase url = [https://ssmidterm-17968.firebaseapp.com]
* gitlab url   = [https://105000066.gitlab.io/Midterm_Project]
* Used bootstrap for responsive design
* Has main three function, browse posts, upload posts and edit your own profile page.
* Used WoW api to add animation when loading page, changing between pages.(ref http://mynameismatthieu.com/WOW/index.html)
* Sign-in page
    * use email to sign up or sign in. Can also use google auth to sign in.
    * Has loading page animation.
    * alert user on successful sign up or sign in.
* Index(Browse)
    * main page, showing post list, showing each post titles.
    * Click on Post title to browse post. 
    * Click on Email to show user profile page.
* Post
    * Enter title and content to submit post.
    * show alert on success submitting post.
* Post page
    * Show the content of post( title and content).
    * Show comments. User can also add comment.
* Profile
    * Show user profile based on user uid.
    * Show user email, profile picture, description about him/herseft, and his/her recent posts.
    * Authorized user can edit description about him/herself or profile picture.
    * Non-authorized user can only view, and can't edit.
* Logout
    * click log out button to log out.
    * Alert user on successful log out.

* Reference
    * lab-6 simple forum
    * firebase Codelab examples


## Security Report (Optional)
* User cannot see the contents of the forum unless they sign in.(will always redirect to sign in page if they are not logged in)
* If not authorized user cannot change his profile(about and profile image).